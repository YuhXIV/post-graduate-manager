﻿using Newtonsoft.Json;
using post_graduate_manager.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace post_graduate_manager
{
    public partial class Form1 : Form
    {
        private PGMDBContext pGMDBContext;
        private int SelectedSupervisorCellKey;
        private int SelectedStudentCellKey;
        private DataTable studentTable;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pGMDBContext = new PGMDBContext();
            List<Superviser> supervisors = pGMDBContext.Supervisers.ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID"));
            dt.Columns.Add(new DataColumn("Name"));
            foreach (Superviser supervisor in supervisors)
            {
                dt.Rows.Add(supervisor.Id, supervisor.Name);
                
            }
            SupervisorsInfoGrid.DataSource = dt;
            SupervisorsInfoGrid.Columns["ID"].Width = 20;
            List<Student> students = pGMDBContext.Students.ToList();
            studentTable = new DataTable();
            studentTable.Columns.Add(new DataColumn("ID"));
            studentTable.Columns.Add(new DataColumn("Name"));
            studentTable.Columns.Add(new DataColumn("Supervisor"));
            foreach (Student student in students)
            {
                studentTable.Rows.Add(student.Id, student.Name, student.Superviser.Name);
            }
            StudentInfoGrid.DataSource = studentTable;
            StudentInfoGrid.Columns["ID"].Width = 20;

        }
       
        private void CreateButton_Click(object sender, EventArgs e)
        {
            if(SelectedSupervisorCellKey == 0)
            {
                MessageBox.Show("Please choose a supervisor");
                return;
            }
            Student student = new Student
            {
                Name = StudentNameField.Text,
                Superviser = pGMDBContext.Supervisers.Find(SelectedSupervisorCellKey),
            };
            pGMDBContext.Students.Add(student);
            pGMDBContext.SaveChanges();
            UpdateStudentGrid();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (SelectedStudentCellKey == 0)
            {
                MessageBox.Show("Please choose a student");
                return;
            }
            pGMDBContext.Students.Remove(pGMDBContext.Students.Find(SelectedStudentCellKey));
            pGMDBContext.SaveChanges();
            UpdateStudentGrid();
        }

      

        private void SerializeBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void SerializeButton_Click(object sender, EventArgs e)
        {
            List<Superviser> supervisors = pGMDBContext.Supervisers.ToList();
            string JSONString = "";
            foreach (Superviser supervisor in supervisors)
            {
                JSONString += JsonConvert.SerializeObject(supervisor);
            }
            SerializeBox.Text = JSONString;
        }  

        #region DataGrid
        private void SupervisorsInfoGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedSupervisorCellKey = int.Parse(SupervisorsInfoGrid.Rows[SupervisorsInfoGrid.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }
        private void StudentInfoGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedStudentCellKey = int.Parse(StudentInfoGrid.Rows[StudentInfoGrid.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }
        #endregion
        /// <summary>
        /// Update the student grid
        /// </summary>
        public void UpdateStudentGrid()
        {
            studentTable = new DataTable();
            studentTable.Columns.Add(new DataColumn("ID"));
            studentTable.Columns.Add(new DataColumn("Name"));
            studentTable.Columns.Add(new DataColumn("Supervisor"));
            List<Student> students = pGMDBContext.Students.ToList();
            foreach (Student student in students)
            {
                studentTable.Rows.Add(student.Id, student.Name, student.Superviser.Name);
            }
            StudentInfoGrid.DataSource = studentTable;
        }
    }
}
