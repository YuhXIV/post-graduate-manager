﻿namespace post_graduate_manager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SupervisorsInfoGrid = new System.Windows.Forms.DataGridView();
            this.ReadButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.StudentNameField = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.StudentNameLabel = new System.Windows.Forms.Label();
            this.StudentInfoGrid = new System.Windows.Forms.DataGridView();
            this.SerializeBox = new System.Windows.Forms.RichTextBox();
            this.SerializeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorsInfoGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentInfoGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // SupervisorsInfoGrid
            // 
            this.SupervisorsInfoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupervisorsInfoGrid.Location = new System.Drawing.Point(817, 68);
            this.SupervisorsInfoGrid.Name = "SupervisorsInfoGrid";
            this.SupervisorsInfoGrid.RowHeadersWidth = 62;
            this.SupervisorsInfoGrid.RowTemplate.Height = 28;
            this.SupervisorsInfoGrid.Size = new System.Drawing.Size(280, 272);
            this.SupervisorsInfoGrid.TabIndex = 0;
            this.SupervisorsInfoGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SupervisorsInfoGrid_CellClick);
            // 
            // ReadButton
            // 
            this.ReadButton.Location = new System.Drawing.Point(968, 356);
            this.ReadButton.Name = "ReadButton";
            this.ReadButton.Size = new System.Drawing.Size(129, 45);
            this.ReadButton.TabIndex = 2;
            this.ReadButton.Text = "Read";
            this.ReadButton.UseVisualStyleBackColor = true;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(668, 356);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(129, 45);
            this.DeleteButton.TabIndex = 3;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.Location = new System.Drawing.Point(174, 88);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(129, 45);
            this.CreateButton.TabIndex = 4;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(533, 356);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(129, 45);
            this.UpdateButton.TabIndex = 5;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            // 
            // StudentNameField
            // 
            this.StudentNameField.Location = new System.Drawing.Point(107, 44);
            this.StudentNameField.Name = "StudentNameField";
            this.StudentNameField.Size = new System.Drawing.Size(196, 26);
            this.StudentNameField.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.StudentNameLabel);
            this.groupBox1.Controls.Add(this.StudentNameField);
            this.groupBox1.Controls.Add(this.CreateButton);
            this.groupBox1.Location = new System.Drawing.Point(32, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 155);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student info";
            // 
            // StudentNameLabel
            // 
            this.StudentNameLabel.AutoSize = true;
            this.StudentNameLabel.Location = new System.Drawing.Point(16, 47);
            this.StudentNameLabel.Name = "StudentNameLabel";
            this.StudentNameLabel.Size = new System.Drawing.Size(51, 20);
            this.StudentNameLabel.TabIndex = 10;
            this.StudentNameLabel.Text = "Name";
            // 
            // StudentInfoGrid
            // 
            this.StudentInfoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StudentInfoGrid.Location = new System.Drawing.Point(366, 68);
            this.StudentInfoGrid.Name = "StudentInfoGrid";
            this.StudentInfoGrid.RowHeadersWidth = 62;
            this.StudentInfoGrid.RowTemplate.Height = 28;
            this.StudentInfoGrid.Size = new System.Drawing.Size(431, 272);
            this.StudentInfoGrid.TabIndex = 8;
            this.StudentInfoGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StudentInfoGrid_CellClick);
            // 
            // SerializeBox
            // 
            this.SerializeBox.Enabled = false;
            this.SerializeBox.Location = new System.Drawing.Point(32, 220);
            this.SerializeBox.Name = "SerializeBox";
            this.SerializeBox.Size = new System.Drawing.Size(236, 181);
            this.SerializeBox.TabIndex = 9;
            this.SerializeBox.Text = "";
            this.SerializeBox.TextChanged += new System.EventHandler(this.SerializeBox_TextChanged);
            // 
            // SerializeButton
            // 
            this.SerializeButton.Location = new System.Drawing.Point(274, 220);
            this.SerializeButton.Name = "SerializeButton";
            this.SerializeButton.Size = new System.Drawing.Size(86, 181);
            this.SerializeButton.TabIndex = 10;
            this.SerializeButton.Text = "Serialize";
            this.SerializeButton.UseVisualStyleBackColor = true;
            this.SerializeButton.Click += new System.EventHandler(this.SerializeButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 432);
            this.Controls.Add(this.SerializeButton);
            this.Controls.Add(this.SerializeBox);
            this.Controls.Add(this.StudentInfoGrid);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.ReadButton);
            this.Controls.Add(this.SupervisorsInfoGrid);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorsInfoGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentInfoGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView SupervisorsInfoGrid;
        private System.Windows.Forms.Button ReadButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.TextBox StudentNameField;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label StudentNameLabel;
        private System.Windows.Forms.DataGridView StudentInfoGrid;
        private System.Windows.Forms.RichTextBox SerializeBox;
        private System.Windows.Forms.Button SerializeButton;
    }
}

