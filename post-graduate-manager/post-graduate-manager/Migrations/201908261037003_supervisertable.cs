namespace post_graduate_manager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class supervisertable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Supervisers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Supervisers");
        }
    }
}
