// <auto-generated />
namespace post_graduate_manager.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class superviserListStudent : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(superviserListStudent));
        
        string IMigrationMetadata.Id
        {
            get { return "201908270934276_superviserListStudent"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
