namespace post_graduate_manager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataDescription : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisers(Name) VALUES('Huy')");
            Sql("INSERT INTO Supervisers(Name) VALUES('Orjan')");
            Sql("INSERT INTO Supervisers(Name) VALUES('Markus')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisers WHERE Name ='Huy'");
            Sql("DELETE FROM Supervisers WHERE Name ='Orjan'");
            Sql("DELETE FROM Supervisers WHERE Name ='Markus'");
        }
    }
}
