namespace post_graduate_manager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class studenttable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Superviser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisers", t => t.Superviser_Id)
                .Index(t => t.Superviser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "Superviser_Id", "dbo.Supervisers");
            DropIndex("dbo.Students", new[] { "Superviser_Id" });
            DropTable("dbo.Students");
        }
    }
}
