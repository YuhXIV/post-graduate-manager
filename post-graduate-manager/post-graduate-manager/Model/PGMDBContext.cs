﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace post_graduate_manager.Model
{
    class PGMDBContext : DbContext
    {
        public DbSet<Superviser> Supervisers { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
